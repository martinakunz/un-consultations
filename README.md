# UN Consultations

This repository is for collecting and sharing drafts and other materials for UN consultations I participate in as the sole or lead author.

My preferred editor is [Emacs Orgmode](https://orgmode.org) and I currently don't have a real-time collaborative document editing system set up, so git-based collaboration and feedback elicitation seems best.

Suggested edits and feedback can be sent via email, a git merge request, or by raising an issue (see sidebar).

Any of my writing published in this repository is shared under the Creative Commons Attribution license [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/), and any of my code under the GNU Affero General Public License [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.html). Contributors to this repository consent to sharing their contributions under the same licenses.

The .org files of the respective folders contain the details of the public consultation in sections that are commented out or otherwise excluded from the export to pdf.
