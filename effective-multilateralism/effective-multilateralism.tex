% Created 2022-08-12 Fri 17:49
% Intended LaTeX compiler: pdflatex
\documentclass[a4paper,11pt,notitlepage]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage[style=oscola,caseshorthands=italic,ibidstyle=uc,maxcitenames=3,backend=biber,indexing=cite] {biblatex}
\usepackage[style=british]{csquotes}
\addbibresource{/home/martina/Documents/Bibliography/references.bib/references.bib}
\addbibresource{/home/martina/git/treaty-references/treatyrefs.bib}
\usepackage{imakeidx}
\usepackage[bottom,hang]{footmisc}
\usepackage[a4paper,top=1.8cm,right=2.4cm,bottom=1.7cm,left=2.4cm,footskip=0.6cm]{geometry}
\usepackage{lmodern}
\usepackage[scaled=0.7]{beramono}
\usepackage[small]{titlesec}
\usepackage[skip=3pt,font=small]{caption}
\usepackage{tabulary,booktabs}
\usepackage{etoolbox}
\usepackage{float}
\usepackage{enumitem}
\setlist{nosep}
\setcounter{secnumdepth}{2}
\date{2022-08-12}
\title{Submission to Public Consultation by High-Level Advisory Board on Effective Multilateralism}
\hypersetup{
 pdfauthor={Martina Kunz, PhD Candidate, University of Cambridge},
 pdftitle={Submission to Public Consultation by High-Level Advisory Board on Effective Multilateralism},
 pdfkeywords={international law, treaties, data science, existential risks},
 pdfsubject={Submission to Public Consultation by High-Level Advisory Board on Effective Multilateralism},
 pdfcreator={Emacs 27.1 (Org mode 9.5.4)}, 
 pdflang={English}}
\begin{document}

\begin{center}
\Large\textbf{Submission to Public Consultation of \\ High-Level Advisory Board on Effective Multilateralism}\par
\end{center}

\normalsize\centerline{Martina Kunz, PhD Candidate, University of Cambridge}

\centerline{12 August 2022}

\section{Areas of global concern most in need of governance improvements}
\label{sec:orgead903b}
Overall, existential risks are the area where governance improvements are most needed in my view, i.e. risks of human extinction due to any number or combination of causes, such as nuclear war, extreme climate change, geoengineering mistakes, ecological and/or global food system collapse, natural or engineered pandemics, asteroid impacts and super-volcanic eruptions, among others. Some of these risks are difficult to mitigate, but others less so, and it might be easiest to start with filling gaps in governance of non-military threats. 

I am encouraged by the Secretary-General's proposals for addressing major risks in Our Common Agenda, including the Futures Lab, the Strategic Foresight and Global Risk Report, and the Emergency Platform. Our capacity to anticipate, identify, monitor, mitigate and respond to known or as-yet unknown existential risks is greatly in need of strengthening. All our global goals, rights and obligations are predicated on the continued existence of our species, and we owe it to present and future generations to take all reasonable, cost-effective measures to reduce or, if possible, eliminate existential threats. If we are successful, humanity may continue to survive and thrive on this planet for another 100'000 years or more. If we fail, this century could be our last. 

\section{Achievable governance improvements and effective multilateralism}
\label{sec:org470e6d0}
Rather than suggesting ways to fill gaps in existential risk governance, the feasibility of which depends to a large extent on political will and funding, for this section I will take a broader perspective and make recommendations for improving international governance of global challenges in general through better \textbf{information management, data science, and automation}.

I applaud the Secretary-General's vision of a `United Nations 2.0' and the new \href{https://www.un.org/en/content/datastrategy/index.shtml}{Data Strategy}, and will hereafter outline what I believe to be achievable and impactful improvements in the context of treaty data. This is based on my PhD research into effective treaty design for environmental problem-solving (not published yet) and a survey of global AI governance efforts.\footnote{\url{https://globalAIgov.org} (draft version), and \cite{Kunz2021_AI-Robotization}.}

Needless to say, treaties are the bedrock of international cooperation and governance. Even small improvements to treaty-making and implementation could have large aggregate effects. The United Nations Treaty Series (UNTS) contains records of over 5000 multilateral agreements (including supplementary protocols and amendments). Clearly, traditional analytical tools are inadequate for gaining an overview of such a treasure trove of data. However, the information contained therein is not currently organized for efficient search, retrieval and analysis. Moreover, the necessary data to track progress towards treaty objectives is often not collected, incomplete, not machine-readable, or not sufficiently fine-grained. Thus, large-scale analysis of implementation and effectiveness is currently impossible.

As the subsequent sections may be too specific, here are the main points:
\begin{itemize}
\item The effectiveness of multilateralism for global problem-solving can only be assessed with better data, which is currently unavailable and/or inaccessible
\item Improving subject matter indexing and access to machine-readable treaty texts in UNTS would facilitate research and discoverability
\item Automated updating of treaty participation data in UNTS would save time, enable higher quality research and prevent misunderstandings
\item Treaty compliance and effectiveness reporting should be automated and resulting data structured so as to allow for large-scale comparative analysis across treaties and parties
\end{itemize}

\subsection{Improving treaty search and access}
\label{sec:orge0971e4}
Treaty search efficiency lags far behind other kinds of searches. Within minutes I can find all open pharmacies near my location, bus schedules of Lagos, Nigeria, or the current weather in Ulaanbaatar, Mongolia. Finding all multilateral treaties mentioning climate change would take much longer. Specifically, I would need to download over 3000 UNTS volumes containing treaty texts, run some natural language processing algorithms including optical character recognition before even getting to full-text search. Moreover, `climate change' is of course not the only term referring to the concept of interest, there are many others. UNTS online offers searching by subject terms, but `climate change' or even `climate' is not in the long list of keywords which includes `desalination' and `dredging'.\footnote{See \url{https://treaties.un.org/Pages/AdvanceSearch.aspx?tab=UNTS\&clang=\_en} (accessed 2022-08-10) -- Filter by: Subject term.} Other entities in the UN system as well as NGOs and academics have acted on the need to provide better treaty information management, but in this case even the most comprehensive and active efforts fall short. My own information extraction algorithm found the following 9 treaties mentioning climate change within a set of about 120 global treaties:

\begin{table}[H]
\caption{\label{tab:orgd92512a}Mentions of climate change in global treaties}
\centering
\small
\begin{tabular}{rlr}
\toprule
Year & Name & Mentions\\
\midrule
1985 & Ozone Layer Protection Convention & 3\\
1992 & Framework Convention on Climate Change & 86\\
1994 & Convention to Combat Desertification & 2\\
1997 & Kyoto Protocol & 49\\
2006 & International Tropical Timber Agreement & 1\\
2009 & Statute of the International Renewable Energy Agency & 3\\
2010 & CBD Access and Benefit-Sharing Protocol & 2\\
2012 & Global Green Growth Institute Establishment Agreement & 2\\
2015 & Paris Agreement & 50\\
\bottomrule
\end{tabular}
\end{table}

The three primary climate change treaties clearly stand out in terms of the number of mentions, but the fact is that a single reference can be enough to create a basis for action (e.g. limiting HFCs under the Montreal Protocol). One of the major domain-specific databases, \href{https://www.ecolex.org/}{Ecolex}, a joint effort by FAO, UNEP and IUCN, has `climate change' as a keyword to filter treaties by,\footnote{See \url{https://www.ecolex.org/result/?q=\&type=treaty\&xkeywords=climate+change\&xdate\_min=\&xdate\_max=\&tr\_type\_of\_document=Multilateral\&tr\_field\_of\_application=Global} (accessed 2022-08-10).} but omits three of the nine agreements listed in Table \ref{tab:orgd92512a}, namely the Ozone Layer Protection Convention,\footcite{OzoneLayerConv} the Convention to Combat Desertification,\footcite{UNCCD} and the IRENA Statute.\footcite{IRENAstatute} A more recent UN system initiative, \href{https://www.informea.org}{InforMEA}, lists the core climate and ozone treaties under its high-level `Climate and atmosphere' category, but free-text search and glossary term search lead to \href{https://www.informea.org/en/search?f\%5B0\%5D=type\%3Atreaty\&text=climate\%20change}{absent} \href{https://www.informea.org/en/search?f\%5B0\%5D=type\%3Atreaty\&f\%5B1\%5D=field\_type\_of\_text\%3A816\&f\%5B2\%5D=field\_leo\_tags\%3A10514}{or} \href{https://www.informea.org/en/knowledge/glossary/climate-change}{incomplete} results.\footnote{This could be a temporary glitch due to a change in their indexing system, but some relevant treaties are not in their database (yet).} 

Especially a monumental challenge like climate change governance requiring broad-based societal, political and economic support on a global level should be made as easily accessible and understandable as possible to any interested students, scholars, activists, philanthropists, investors and concerned citizens.
Rather than setting up ever more domain-specific treaty databases, it would be most convenient and efficient for the international community if the UN Treaty Section were to upgrade its treaty publication services in the spirit of a UN 2.0.

\subsubsection*{Specific recommendations:}
\label{sec:orge8d17e8}
\begin{itemize}
\item Provide free-text search of treaties (both original and translated versions where available)
\item Enable efficient download of machine-readable treaty texts (via a bulk download option or an application programming interface) for further analysis and merging with other data sources
\begin{itemize}
\item An intermediate improvement would be to publish agreement texts in html version (without prohibiting programmatic access to treaty pages)
\end{itemize}
\item Publish texts and metadata under an Open Data license allowing for re-use and derivative works, e.g. \href{https://creativecommons.org/licenses/by/3.0/igo/}{CC BY 3.0 IGO}
\item Upgrade search by subject term:
\begin{itemize}
\item Perhaps starting by manually adding or double-checking the indexing of major global challenges (and/or improve it based on an analysis of the most frequent free-form title search queries)
\item Ideally, replace or complement with results of an automated topic modelling algorithm based on full treaty texts
\item Provide a way for researchers or other interested parties to suggest additional subject terms or search patterns (e.g. a regular expression)\footnote{Regular expressions provide a very efficient way to specify search patterns for text and are a standard module in many programming languages.} to tag treaties with
\end{itemize}
\end{itemize}

\subsection{Providing more complete and up-to-date treaty actions data}
\label{sec:org7592bdf}

Data on signatures, ratifications, accessions, withdrawals, reservations and suchlike is essential for the assessment of treaty applicability, geographic reach, membership growth and other patterns. For instance, the UNTS treaty page of the above-mentioned IRENA Statute lists 30 ratifications as the only treaty actions data,\footnote{\url{https://treaties.un.org/Pages/showDetails.aspx?objid=08000002802aefa2\&clang=\_en} (accessed 2022-08-10).} while the IRENA home page displays \href{https://www.irena.org/irenamembership}{168 members}.\footnote{\url{https://www.irena.org/irenamembership} (accessed 2022-08-10).} The responsibility to register the change in membership with the UNSG lies with the depositary government (Germany). But treaties deposited with UN specialized agencies, in particular IMO and ICAO, often have incomplete/outdated UNTS treaty pages as well.
From a big data analytics perspective, a \textbf{one-stop-shop for treaty data} would be of immense help. Moreover, incomplete information is sometimes worse than no information, especially when provided by an authoritative source. Newcomers, superficially interested and/or time-constrained individuals may take UNTS data at face value without verifying it against data published by the respective treaty secretariat or depositary. Someone looking into international cooperation on renewable energy might dismiss IRENA as a minor, small-scale organization based on UNTS data, when in reality it has global reach.

While initial registration of agreements will probably continue to require manual processing by UN Treaty Section staff, even if submitted online in the future,\footnote{UNGA Resolution A/RES/76/120 para 10 encourages the development of an online treaty registration system as an alternative to submission of electronic or paper documentation.} subsequent updates seem more feasible to automate, especially within the UN system. If immediate, automatic updates pose problems for whatever reason, a weekly or monthly synchronization of data would already be a significant improvement over the status quo.

Relatedly, for data science purposes it would be ideal if treaty participants had a unique identifier. Currently their names vary across UNTS treaty pages and between different UN treaty databases. Sometimes this is because the concerned states or other entities changed their official names over time, and what is registered is the name they had at the time the information was entered into the database, but often times it seems to be due to preferences, habits or policies of the agency publishing the information. This understandable diversity means unfortunately that analysis of networks and participation patterns requires a significant upfront effort to clean up the data and consequently limits the number of agreements that can be covered within a given research project. There is no need to change the participant names listed in UNTS treaty pages, the historical record can remain as is, simply adding a column with a unique identifier that is used consistently throughout the database would suffice.

\subsubsection*{Specific recommendations:}
\label{sec:org6f30fb6}
\begin{itemize}
\item All entities within the UN system acting as treaty depositaries should automatically update the respective UNTS entries
\item Member states acting as treaty depositaries should automatically forward updates to the respective UNTS records as frequently as possible and should receive technical support to set this up
\item Ideally, treaty participants should have unique identifiers used across all UNTS treaty records (and beyond)
\end{itemize}

\subsection{Towards automated, data-driven compliance \& effectiveness monitoring}
\label{sec:org210ec1b}
Access to high-quality treaty texts and participation data allows for large-scale analysis of international legal and political success of multilateralism. However, for many treaties we can ultimately only judge effectiveness by looking at (socio-)economic, environmental or other outcome data. Most compliance and effectiveness monitoring processes of multilateral treaties are currently infrequent and require a very large amount of person-hours, if undertaken at all. Setting up increasingly automated national and international reporting processes where possible would need a certain initial effort but would likely pay off for all stakeholders and free up resources for further governance improvements. Free and open source software and open data formats should be widely adopted in an inclusive and participatory fashion.

As the COVID-19 pandemic has shown, timely data on the evolution of outcomes and on the effectiveness of policy measures can be instrumental for faster adaptation and for saving lives and livelihoods. Linking regulatory data with outcome data is possible but would benefit from a concerted effort across data providers to facilitate semantic interoperability and data integration. The UN is best placed to develop and mainstream best practices across multilateral treaty systems and should receive funding for this purpose.
\end{document}
